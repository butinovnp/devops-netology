Благодаря .gitignore в каталоге terraform будут проигнорированны:
- файлы содержащие в имени tfstate
- файл с именем crash.log
- все файлы с расширением .tfvars
- все файлы с именем override.tf и override.tf.json, а так же файлы, название которых оканчивается на _override.tf или _override.tf.json
- файлы с именами .terraformrc и terraform.rc